from subprocess import run, TimeoutExpired
from partition import partition
import sys
import os
import datetime
import json

if len(sys.argv) not in [6, 7]:
    print("usage:", sys.argv[0], "seed-dir #workers duration[s] id ram [additional-args]")
    exit(1)

PATH = os.environ["PATH"]
if "llvm" not in PATH:
    print("modules not loaded")
    exit(1)

seed = sys.argv[1]
if "x" in sys.argv[2]:
    instances, workers_per_instance = sys.argv[2].split("x")
    instances = int(instances)
    workers_per_instance = int(workers_per_instance)
else:
    instances = int(sys.argv[2])
    workers_per_instance = 1

total_workers = instances * workers_per_instance
duration = int(sys.argv[3])
id = sys.argv[4]
args = "[]"
assert sys.argv[5][-1] == "G"
ram = int(sys.argv[5][:-1])
if len(sys.argv) == 7:
    args = sys.argv[6]
    json.loads(args)

mins = (duration + 59 + 300) // 60
hrs = str(mins // 60).zfill(2)
mins = str(mins % 60).zfill(2)

if os.path.basename(seed) == "":
    seed = os.path.dirname(seed)
partition_path = os.path.join(os.path.dirname(seed), os.path.basename(seed) + "-partitioned")
partition(seed, partition_path, total_workers)

HOME = os.environ["HOME"]
os.makedirs(os.path.join(HOME, "mcruns", id))

for instance in range(instances):
    workers = ",".join([str(instance * workers_per_instance + i) for i in range(workers_per_instance)])
    run([
        "bsub",
        "-W", f"{hrs}:{mins}",                                                                      # duration
        "-n", str(workers_per_instance),                                                            # num cpus
        "-R", f"rusage[mem={(1024 * ram + workers_per_instance - 1) // workers_per_instance}]",     # RAM
        "-R", "rusage[scratch=2048]",                                                               # scratch space
        "python3", "wrapper.py",                                                                    # command
            os.path.join(partition_path, str(total_workers)),                                       # seeds
            id,                                                                                     # run id
            workers,                                                                                # workers for this job
            str(duration),                                                                          # duration
            str(ram * 1000000),                                                                     # ram limit per clang instance
            args                                                                                    # additional arguments for fuzzer
    ])
