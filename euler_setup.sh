#!/usr/bin/bash
cd ~/
git clone https://gitlab.ethz.ch/sandris/ast-project
mv ast-project meta-compiler
tar xfv meta-compiler/jre-8u333-linux-x64.tar.gz
cp meta-compiler/*.py ~/
cp -r ~/meta-compiler/mods ~/
cd /cluster/scratch/$USER/
tar -xzvf ~/meta-compiler/error-fuzzer/gcc-seed-programs.tgz
source ~/meta-compiler/comp-mods
cd ~/meta-compiler/error-fuzzer
bash ./build.sh
cd ~
python3 launch.py /cluster/scratch/$USER/gcc-seed-programs/ 14 1800 memlimit-testrun1 2G
