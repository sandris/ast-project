import os
import sys
from shutil import copyfile

def partition(srcdir, dstdir, cnt, force=False):
    next = 0

    if os.path.exists(dstdir + "/" + str(cnt)) and not force:
        return

    if not srcdir.endswith("/"):
        srcdir += "/"

    def has_extension(file, extensions):
        for extension in extensions:
            if file.endswith("." + extension):
                return True
        return False

    def copy(path, cnt, idx=None):
        if idx == None:
            idx = "all"
        else:
            idx = str(cnt) + "/" + str(idx)
        dst = os.path.join(dstdir, idx, path)
        try:
            os.makedirs(os.path.dirname(dst))
        except FileExistsError:
            pass
        copyfile(os.path.join(srcdir, path), dst)

    for root, dirs, files in os.walk(srcdir):
        assert root.startswith(srcdir)
        for file in files:
            path = os.path.join(root, file)[len(srcdir):]
            copy(path, cnt)
            if has_extension(file, {"c", "cpp", "cc", "C"}):
                copy(path, cnt, next)
                next = (next + 1) % cnt
            elif has_extension(file, {"h", "hpp", "H"}):
                for i in range(cnt):
                    copy(path, cnt, i)
            else:
                print("unsupported extension for file", file)
                exit(1)

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("usage:", sys.argv[0], "src dst #partitions")
        exit(1)
    srcdir = sys.argv[1]
    dstdir = sys.argv[2]
    cnt = int(sys.argv[3])
    partition(srcdir, dstdir, cnt)