package edu.ucdavis.error.fuzzer.test_main;

import org.junit.Before;
import org.junit.Test;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;

import edu.ucdavis.error.fuzzer.core.Main;

public class TestMain {

  @Before
  public void setup() {
    Configurator.defaultConfig().level(Level.INFO).activate();
  }


  public void dummyMain() {

    Main.main(new String[] { "--engine", "crazy", "--num-threads", "1",
        "--seed-folder", "gcc-seed-programs" });
  }

}
