package edu.ucdavis.error.fuzzer.reducer;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.repository.ErrorInstance;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

/** Created by Chengnian Sun on 2/27/17. */
@RunWith(JUnit4.class)
public class ErrorReducerTest {

  private static final File ERROR_INSTANCE_FOLDER = new File("test-data/error-instances/");

  private static void ensureReducerCanRun(File instanceFolder) {
    final ErrorInstance instance = ErrorInstance.load(instanceFolder);
    Preconditions.checkState(!instance.checkIntegrity().hasProblems());
    final ErrorReducer reducer = new ErrorReducer(instance);
    reducer.reduce();
  }

  //  @Test
  public void testReducerCanRun() {
    ImmutableList.<String>builder()
        .add("MSG_no_type_named_.__in_._/000")
        .add("MSG_array_type_.__is_not_assignable/000")
        .build()
        .stream()
        .map(name -> new File(ERROR_INSTANCE_FOLDER, name))
        .forEach(ErrorReducerTest::ensureReducerCanRun);
  }

  /**
   * This test case is expected to fail, because the clang-foramt does not handle trigraph very
   * well.
   *
   * <p>So, ignore this test case.
   */
  public void testTwo() {
    final ErrorInstance instance =
        ErrorInstance.load(
            new File(
                ERROR_INSTANCE_FOLDER,
                "MSG_second_parameter_of__main____argument_array___must_be_of_type_._/001"));
    Preconditions.checkState(!instance.checkIntegrity().hasProblems());
    try {
      final ErrorReducer reducer = new ErrorReducer(instance);
      reducer.reduce();
    } catch (Throwable e) {
      e.printStackTrace();
      Assert.fail();
    }
  }

  @Test
  public void testFormatBugTwo() throws IOException {
    final File folder =
            new File("test-data/reduction-test-data/incorrect-placement-of-identifiers-2");
    final List<String> goodTokenList =
            Files.readAllLines(new File(folder, "good_reduced_lined" + ".c").toPath());
    final List<String> badTokenList =
            Files.readAllLines(new File(folder, "bad_reduced_lined.c").toPath());
    final File formattedSourceFile = new File(folder, "good_reduced_formatted.c");
    final String formatBadSourceCode =
            ErrorReducer.formatBadSourceCode(
                    new ErrorReducer.ExamplePair(goodTokenList, badTokenList), formattedSourceFile);
    assertThat(formatBadSourceCode).isEqualTo("operator typedef int pfn;");
  }

  @Test
  public void testFormatBugOne() throws IOException {
    final File folder =
        new File("test-data/reduction-test-data/incorrect-placement-of" + "-identifiers");
    final List<String> goodTokenList =
        Files.readAllLines(new File(folder, "good_reduced_lined" + ".c").toPath());
    final List<String> badTokenList =
        Files.readAllLines(new File(folder, "bad_reduced_lined.c").toPath());
    final File formattedSourceFile = new File(folder, "good_reduced_formatted.c");
    final String formatBadSourceCode =
        ErrorReducer.formatBadSourceCode(
            new ErrorReducer.ExamplePair(goodTokenList, badTokenList), formattedSourceFile);
    assertThat(formatBadSourceCode).isEqualTo("int b = (static 1);");
  }

  @Test
  public void testFormatBadSourceCode() throws IOException {
    final File folder = new File("test-data/reduction-test-data/comments_added_by_clang_format");
    final List<String> goodTokenList =
        Files.readAllLines(new File(folder, "good_reduced_lined.C").toPath());
    final List<String> badTokenList =
        Files.readAllLines(new File(folder, "bad_reduced_lined.C").toPath());
    final File formattedSourceFile = new File(folder, "good_reduced_formatted.C");
    final String formatBadSourceCode =
        ErrorReducer.formatBadSourceCode(
            new ErrorReducer.ExamplePair(goodTokenList, badTokenList), formattedSourceFile);
    final String expectedFormattedBadSourceCode =
        com.google.common.io.Files.asCharSource(new File(folder, "golden_file.C"), Charsets.UTF_8)
            .read();
    assertThat(formatBadSourceCode).isEqualTo(expectedFormattedBadSourceCode);
  }
}
