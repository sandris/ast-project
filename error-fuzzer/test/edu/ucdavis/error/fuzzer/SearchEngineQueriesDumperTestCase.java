package edu.ucdavis.error.fuzzer;

import edu.ucdavis.error.fuzzer.util.SearchEngineQueriesDumper;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by neo on 2/19/17.
 */
public class SearchEngineQueriesDumperTestCase {

  @Test
  public void test() {
    final String pattern = "\\[rewriter\\] .+";
    final List<String> result = SearchEngineQueriesDumper.constructGoogleQueryFromPattern(pattern);
    assertEquals(1, result.size());
    assertEquals("[rewriter]", result.get(0));
  }

  @Test
  public void unescapeParentheses() {
    final String pattern = "cannot initialize a vector element of type .+ with an rvalue of type " +
            "" + ".+: different qualifiers \\(const vs restrict\\)";
    final List<String> result = SearchEngineQueriesDumper.constructGoogleQueryFromPattern(pattern);
    assertEquals(3, result.size());
    assertEquals("cannot initialize a vector element of type", result.get(0));
    assertEquals("with an rvalue of type", result.get(1));
    assertEquals(": different qualifiers (const vs restrict)", result.get(2));
  }

  @Test
  public void unescapeDots() {
    final String pattern = "test module file extension '.+' has different version \\(.+\\..+\\) "
            + "than expected \\(.+\\..+\\)";
    final List<String> result = SearchEngineQueriesDumper.constructGoogleQueryFromPattern(pattern);
    assertEquals(6, result.size());
    assertEquals("test module file extension '", result.get(0));
    assertEquals("' has different version (", result.get(1));
    assertEquals(".", result.get(2));
    assertEquals(") than expected (", result.get(3));
    assertEquals(".", result.get(4));
    assertEquals(")", result.get(5));
  }

  @Test
  public void unescapeDash() {
    final String pattern = "'expected\\-no\\-diagnostics' directive cannot follow " +
            "'expected\\-no\\-diagnostics' directive";
    final List<String> result = SearchEngineQueriesDumper.constructGoogleQueryFromPattern(pattern);
    assertEquals(1, result.size());
    assertEquals("'expected-no-diagnostics' directive cannot follow 'expected-no-diagnostics' " +
            "directive", result.get(0));
  }

  @Test
  public void unescapeStar() {
    final String pattern = "casting .+ to incompatible type .+; remove \\*: different qualifiers " +
            "\\(const vs restrict\\)";
    final List<String> result = SearchEngineQueriesDumper.constructGoogleQueryFromPattern(pattern);

    assertEquals(3, result.size());
    assertEquals("casting", result.get(0));
    assertEquals("to incompatible type", result.get(1));
    assertEquals("; remove *: different qualifiers (const vs restrict)", result.get(2));
  }

  @Test
  public void unescapeMultipleDots() {
    final String pattern = "qualifier contains unexpanded parameter packs .+, .+, \\.\\.\\.";
    final List<String> result = SearchEngineQueriesDumper.constructGoogleQueryFromPattern(pattern);

    assertEquals(3, result.size());
    assertEquals("qualifier contains unexpanded parameter packs", result.get(0));
    assertEquals(",", result.get(1));
    assertEquals(", ...", result.get(2));
  }

  @Test
  public void unescapeQuestionMark() {
    final String pattern = "type trait requires .+ or more argument; have .+ argument.?";
    final List<String> result = SearchEngineQueriesDumper.constructGoogleQueryFromPattern(pattern);

    assertEquals(3, result.size());
    assertEquals("type trait requires", result.get(0));
    assertEquals("or more argument; have", result.get(1));
    assertEquals("argument", result.get(2));
  }

}
