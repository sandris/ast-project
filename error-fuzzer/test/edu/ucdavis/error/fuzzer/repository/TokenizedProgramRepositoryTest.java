package edu.ucdavis.error.fuzzer.repository;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.token.TokenizedProgram;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

import static com.google.common.truth.Truth.assertThat;

/** Created by neo on 4/3/17. */
@RunWith(JUnit4.class)
public class TokenizedProgramRepositoryTest {

  @Test
  public void testConstructTokenizedProgramRepository() {
    final File folder = new File("test-data/tiny-seed-repo");
    final SeedRepository seedRepo = new SeedRepository.Builder(folder).build();
    final TokenizedProgramRepository repo =
        TokenizedProgramRepository.createRepository(
            seedRepo.getSeeds(), new File("temp/tokenized-program-repo"));
    ImmutableList<SourceProgramWithTokens> programs = repo.getPrograms();
    assertThat(programs).hasSize(3);
    try {
      final SourceProgramWithTokens p1 = programs.get(0);
      final TokenizedProgram tk1 =
          TokenizedProgram.tokenizeSourceProgram(p1.getSourceFile().getAbsoluteFile());
      assertThat(p1.getTokenList()).containsExactlyElementsIn(tk1.getTokens()).inOrder();
    } catch (Throwable e) {
      e.printStackTrace();
      Assert.fail();
    }
    try {
      final SourceProgramWithTokens p1 = programs.get(1);
      final TokenizedProgram tk1 =
          TokenizedProgram.tokenizeSourceProgram(p1.getSourceFile().getAbsoluteFile());
      assertThat(p1.getTokenList()).containsExactlyElementsIn(tk1.getTokens()).inOrder();
    } catch (Throwable e) {
      e.printStackTrace();
      Assert.fail();
    }
    try {
      final SourceProgramWithTokens p1 = programs.get(2);
      final TokenizedProgram tk1 =
          TokenizedProgram.tokenizeSourceProgram(p1.getSourceFile().getAbsoluteFile());
      assertThat(p1.getTokenList()).containsExactlyElementsIn(tk1.getTokens()).inOrder();
    } catch (Throwable e) {
      e.printStackTrace();
      Assert.fail();
    }
  }
}
