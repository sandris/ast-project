#!/usr/bin/env bash

set -o pipefail
set -o nounset

if [[ $# != 2 ]] ; then 
  echo "Usage: $0 <gcc testsuite folder> <target folder>"
  exit 1
fi


readonly SOURCE_FOLDER=$1
readonly TARGET_FOLDER=$2
readonly GOODCC="clang"
readonly GOODCXX="clang++"

readonly TIMEOUT_CC=10

if [[ ! -d "${SOURCE_FOLDER}" ]] ; then 
  echo "${SOURCE_FOLDER} is not a folder, or does not exist"
  exit 1
fi

rm -rf   "${TARGET_FOLDER}"
mkdir -p "${TARGET_FOLDER}"

rsync -a --include '*/' --include '*.c' --include '*.cpp' --include '*.cc' \
         --include '*.h' --include '*.H' --include '*.hpp'  --include '*.C' \
         --include '*.CC' --exclude '*' ${SOURCE_FOLDER} \
         ${TARGET_FOLDER}

readonly TEMP_FOLDER="temp"
mkdir -p "${TEMP_FOLDER}"
readonly CPP_FILE="${TEMP_FOLDER}/cpp.temp"
for file in $(find "${TARGET_FOLDER}" -name "*.C" -o -name "*.cpp" -o -name "*.CC" -o -name "*.cc") ; do
  echo "compiling $file"
  ${GOODCXX} -std=c++14 $file -E -P > ${CPP_FILE}
  cp ${CPP_FILE} $file 
  if ! timeout -s 9 ${TIMEOUT_CC} ${GOODCXX} -std=c++14 $file -c -w \
        -o "${TEMP_FOLDER}/temp.o" > /dev/null; then 
    echo "    $file cannot compile, thus removed"
    rm $file
  fi
  echo ""
done


for file in $(find "${TARGET_FOLDER}" -name "*.c") ; do
  echo "compiling $file"
  ${GOODCC} -std=c11 $file -E -P > ${CPP_FILE}
  cp ${CPP_FILE} $file
  if ! timeout -s 9 ${TIMEOUT_CC} ${GOODCC} -std=c11 $file -c -w \
        -o "${TEMP_FOLDER}/temp.o" > /dev/null ; then 
    echo "    $file cannot compile, thus removed"
    rm $file
  fi  
  echo ""
done






