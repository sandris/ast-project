package edu.ucdavis.error.analyzer;

import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.PrintStream;

/**
 * Created by neo on 2/24/17.
 */
public class TextualErrorReportGenerator extends AbstractErrorReportGenerator {

  protected TextualErrorReportGenerator(File errorRepoFolder) {
    super(errorRepoFolder);
  }

  @Override
  protected void writeHead(PrintStream stream) {

  }

  @Override
  protected void writeBottom(PrintStream stream) {

  }

  @Override
  protected void saveExample(int id, PrintStream stream, PairExample example) {
    stream.println("===Example===");
    stream.println("id=" + id);
    stream.println("name=" + example.errorId);

    stream.println("===Section===");
    stream.println(example.errorMsg);

    stream.println("===Section===");
    stream.println("---good---");
    stream.println(example.goodExample);

    stream.println("===Section===");
    stream.println("---bad---");
    stream.println(example.badExample);
  }

  public static void main(String[] args) {
    File expFolder = null;
    if (args.length == 0) {
      expFolder = new File("exp-runs/error/RandomTokenManipulationFuzzingEngine");
      Logger.info("Using default exp folder: {}", expFolder);
    } else if (args.length == 1){
      expFolder = new File(args[0]);
    } else {
      System.err.println(
              "<main> <error folder, eg.: exp-runs/error/IdentifierSubstituionFuzzingEngine");
      System.exit(1);
    }
    TextualErrorReportGenerator gen = new TextualErrorReportGenerator(expFolder);
    File outputFile = new File("test.report.txt");
    gen.save(outputFile);
    Logger.info("saved report to {}", outputFile);

  }
}
