package edu.ucdavis.error.fuzzer.token;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Files;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.core.Constants;
import edu.ucdavis.error.fuzzer.token.cdt.CDTTokenizer;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.pmw.tinylog.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.*;

/** Created by Chengnian Sun on 3/27/17. */
public class TokenizedProgram {

  public static TokenizedProgram tokenizeSourceProgram(File filepath)
      throws ExpansionOverlapsBoundaryException, ParsingFailureException {
    final SourceFile sourceFile = new SourceFile(filepath);
    removePragmas(sourceFile);
    return new TokenizedProgram(new CDTTokenizer(sourceFile).getTokens());
  }

  public static TokenizedProgram tokenizeSourceProgram(String filepath)
      throws ExpansionOverlapsBoundaryException, ParsingFailureException {
    return tokenizeSourceProgram(new File(filepath));
  }

  public static TokenizedProgram readTokenizedProgram(String filepath) throws IOException {
    return readTokenizedProgram(new File(filepath));
  }

  public static TokenizedProgram readTokenizedProgram(File file) throws IOException {
    checkArgument(file.exists() && file.isFile(), "The program file %s does not exist", file);
    final ImmutableList.Builder<TokenProgramSegment> builder = ImmutableList.builder();
    for (String line : Files.readLines(file, Constants.DEFAULT_CHARSET)) {
      final int tabIndex = line.indexOf('\t');
      checkState(tabIndex > 0, "A tab separator is expected");
      final String stringTokenType = line.substring(0, tabIndex).trim();
      final int tokenType = Integer.parseInt(stringTokenType);
      checkNotNull(tokenType, "No token type for the string token type %s", stringTokenType);
      final String lexeme = line.substring(tabIndex + 1).trim();

      builder.add(TokenProgramSegment.createTokenWithNoOffsets(lexeme, tokenType));
    }
    return new TokenizedProgram(builder.build());
  }

  private static void removePragmas(SourceFile file) {
    try {
      final AtomicBoolean hasPragma = new AtomicBoolean(false);
      List<String> lines =
          Files.readLines(file.getAbsoluteFile(), Charsets.UTF_8)
              .stream()
              .filter(
                  line -> {
                    if (line.trim().startsWith("#pragma")) {
                      hasPragma.set(true);
                      return false;
                    } else {
                      return true;
                    }
                  })
              .collect(Collectors.toList());
      if (hasPragma.get()) {
        Files.asCharSink(file.getAbsoluteFile(), Charsets.UTF_8).writeLines(lines, "\n");
      }
    } catch (IOException e) {
      Logger.error(e);
    }
  }

  private final ImmutableList<TokenProgramSegment> tokens;

  public TokenizedProgram(ImmutableList<TokenProgramSegment> tokens) {
    this.tokens = tokens;
  }

  public TokenizedProgram writeTokensToFile(String filepath) throws IOException {
    return this.writeTokensToFile(new File(filepath));
  }

  public ImmutableList<TokenProgramSegment> getTokens() {
    return tokens;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (obj.getClass() != this.getClass()) {
      return false;
    }
    final TokenizedProgram other = (TokenizedProgram) obj;
    return other.tokens.equals(other.tokens);
  }

  @Override
  public int hashCode() {
    return this.tokens.hashCode();
  }

  public TokenizedProgram writeTokensToFile(File file) throws IOException {
    file.getParentFile().mkdirs();
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      for (TokenProgramSegment token : this.tokens) {
        writer
            .append(Integer.toString(token.getTokenType()))
            .append("\t")
            .append(token.getLexeme())
            .append("\n");
      }
    }
    return this;
  }

  public TokenizedProgram outputSourceProgram(String filepath) throws IOException {
    return this.outputSourceProgram(new File(filepath));
  }

  public TokenizedProgram outputSourceProgram(File file) throws IOException {
    file.getParentFile().mkdirs();
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      for (TokenProgramSegment token : this.tokens) {
        writer.append(token.getLexeme()).append("\n");
      }
    }
    return this;
  }
}
