package edu.ucdavis.error.fuzzer.message;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import edu.ucdavis.error.fuzzer.core.ErrorFuzzerFatalError;

public class ClangSourcefileMessageMapping {

  private final Map<String, Set<String>> file2messagesMap;

  /**
   * mapping a message to the files that use the message
   */
  // private final ImmutableMultimap<String, String> msg2FileMap;
  private final Map<String, Set<String>> msg2FileMap;

  private ClangSourcefileMessageMapping(File mappingFile) {
    this.file2messagesMap = parseFile2MsgMapFile(mappingFile);
    ;
    this.msg2FileMap = new HashMap<>();

    this.file2messagesMap.forEach((file, msgs) -> msgs.forEach(msg -> {
      Set<String> set = msg2FileMap.get(msg);
      if (set == null) {
        set = new HashSet<>();
        msg2FileMap.put(msg, set);
      }
      set.add(file);
    }));
  }

  private Map<String, Set<String>> parseFile2MsgMapFile(File file)
      throws ErrorFuzzerFatalError {
    final Map<String, Set<String>> map = new HashMap<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      for (String line = reader.readLine(); line != null; line = reader
          .readLine()) {
        final int pivotIndex = line.indexOf(':');
        final String fileName = line.substring(0, pivotIndex);
        final int lastPivotIndex = line.lastIndexOf(':');
        final String msg = line.substring(lastPivotIndex + 1);
        Set<String> value = map.get(fileName);
        if (value == null) {
          value = new HashSet<>();
          map.put(fileName, value);
        }
        value.add(msg);
      }
    } catch (IOException e) {
      throw new ErrorFuzzerFatalError(e);
    }
    return map;
  }

  public Set<String> getFilesUsingTheMessage(String message) {
    return Collections.unmodifiableSet(this.msg2FileMap.get(message));
  }

  public Set<String> getMessagesInFilesOfInterest() {
    Set<String> result = new HashSet<>();
    this.file2messagesMap.forEach((key, value) -> {
      if (isFileOfInterest(key)) {
        result.addAll(value);
      }
    });
    return result;
  }

  private static final ImmutableList<String> FILTERED_FILE_LIST = ImmutableList
      .of("SemaObjCProperty.cpp", "SemaExprObjC.cpp", "SemaDeclObjC.cpp",
          "SemaCoroutine.cpp", "SemaCUDA.cpp", "CGCUDANV.cpp", "ParseObjc.cpp",
          "ParseOpenMP.cpp");

  private boolean isFileOfInterest(String file) {
    for (String toFilter : FILTERED_FILE_LIST) {
      if (file.contains(toFilter)) {
        return false;
      }
    }
    return true;
  }

  private final static ClangSourcefileMessageMapping DEFAULT = new ClangSourcefileMessageMapping(
      new File("clang-3.9-source/msg-file-mapping.txt"));

  public static ClangSourcefileMessageMapping getDeault() {
    return DEFAULT;
  }

  public static void main(String[] args) {
    ClangSourcefileMessageMapping map = new ClangSourcefileMessageMapping(
        new File("clang-3.9-source/msg-file-mapping.txt"));
    for (Map.Entry<String, Set<String>> entry : map.file2messagesMap
        .entrySet()) {
      System.out.println(entry.getKey());
      System.out.println(entry.getValue());
      // for (String msg : entry.getValue()) {
      // System.out.println(" " + msg);
      // }
    }
  }
}
