package edu.ucdavis.error.fuzzer.message;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.pmw.tinylog.Logger;

import com.google.common.base.Charsets;

import edu.ucdavis.error.fuzzer.message.ClangDiagnosticsMsgTemplate.ErrorMsgTemplateMatchingFailureException;

public class CompilerError {

  private static final File ALL_ERROR_FILE = new File(
      "all_triggered_errors.txt");

  // private static final Map<String, CompilerError> TRIGGERED_ERRORS = new
  // HashMap<>();
  private static final Set<String> TRIGGERED_ERRORS = new HashSet<>();

  static {
    if (ALL_ERROR_FILE.exists()) {
      try (BufferedReader reader = new BufferedReader(
          new FileReader(ALL_ERROR_FILE))) {
        for (String line = reader.readLine(); line != null; line = reader
            .readLine()) {
          TRIGGERED_ERRORS.add(line);
        }
      } catch (FileNotFoundException e) {
        Logger.error(e);
      } catch (IOException e) {
        Logger.error(e);
      }
    }
  }

  private final int lineNo;

  private final int columnNo;

  private final ClangDiagnosticsMsgTemplate template;

  private final String message;

  private final String signature;

  private CompilerError(int lineNo, int columnNo, String message)
      throws ErrorMsgTemplateMatchingFailureException {
    this.lineNo = lineNo;
    this.columnNo = columnNo;
    this.template = ClangDiagnosticsMsgTemplate.matchTemplate(message);
    this.message = message;
    this.signature = getSignature(template, message);
  }

  public int getLineNo() {
    return lineNo;
  }

  public int getColumnNo() {
    return columnNo;
  }

  private String getSignature(ClangDiagnosticsMsgTemplate template,
      String message) {
    return template.getPattern().pattern();
  }

  public ClangDiagnosticsMsgTemplate getTemplate() {
    return template;
  }

  public String getMessage() {
    return message;
  }

  public String getSignature() {
    return signature;
  }

  public static CompilerError parse(String fileName, int lineNo, int columnNo,
      String message) throws ErrorMsgTemplateMatchingFailureException {
    final CompilerError error = new CompilerError(lineNo, columnNo, message);

    final String signature = error.getSignature();
    if (error != null && !TRIGGERED_ERRORS.contains(signature)) {
      try {
        FileUtils.writeStringToFile(ALL_ERROR_FILE, signature + "\n",
            Charsets.UTF_8, true);
        TRIGGERED_ERRORS.add(signature);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
    return error;
  }

}
