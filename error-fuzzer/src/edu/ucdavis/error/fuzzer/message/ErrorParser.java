package edu.ucdavis.error.fuzzer.message;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Preconditions;

import edu.ucdavis.error.fuzzer.message.ClangDiagnosticsMsgTemplate.ErrorMsgTemplateMatchingFailureException;

public class ErrorParser {
  public static class Result {
    public final List<CompilerError> handledErrors = new ArrayList<>();
    public final List<String> unhandledErrors = new ArrayList<>();
  }
  private static final Pattern ERROR_PATTERN = Pattern
      .compile("^(.+):([0-9]+):([0-9]+): (?:fatal )?error: (.+)$");

  private final static int GROUP_FILE = 1;
  private final static int GROUP_LINE = 2;
  private final static int GROUP_COLUMN = 3;
  private final static int GROUP_MSG = 4;

  private static final Pattern WARNING_PATTERN = Pattern
      .compile("^(.+):([0-9]+):([0-9]+): warning: (.+)$");

  public static List<String> parseWarnings(String compilerOutput)
      throws ErrorMsgTemplateMatchingFailureException {
    final List<String> warnings = new ArrayList<>();
    for (String line : compilerOutput.split("(\\r)?\n")) {
      final Matcher matcher = WARNING_PATTERN.matcher(line);
      if (!matcher.matches()) {
        continue;
      }
      Preconditions.checkState(matcher.groupCount() == 4);
      // final String fileName = matcher.group(GROUP_FILE);
      // final int lineNo = Integer.parseInt(matcher.group(GROUP_LINE));
      // final int columnNo = Integer.parseInt(matcher.group(GROUP_COLUMN));
      final String msg = matcher.group(GROUP_MSG)
          .replaceFirst("\\s+\\[\\-W.+\\]$", "").replaceAll("'[^']+'", "''");
      warnings.add(msg);
    }
    return warnings;
  }

  public static Result parseErrors(String compilerOutput) {
    final Result result = new Result();
    for (String line : compilerOutput.split("(\\r)?\n")) {
      final Matcher matcher = ERROR_PATTERN.matcher(line);
      if (!matcher.matches()) {
        continue;
      }
      Preconditions.checkState(matcher.groupCount() == 4);
      final String fileName = matcher.group(GROUP_FILE);
      final int lineNo = Integer.parseInt(matcher.group(GROUP_LINE));
      final int columnNo = Integer.parseInt(matcher.group(GROUP_COLUMN));
      final String msg = matcher.group(GROUP_MSG)
          .replaceFirst("\\s+\\[\\-W.+\\]$", "");
      try {
        result.handledErrors.add(CompilerError.parse(fileName, lineNo, columnNo, msg));
      } catch (ErrorMsgTemplateMatchingFailureException e) {
        result.unhandledErrors.add(e.getUnhandledErrorMsg());
      }
    }
    return result;
  }

}
