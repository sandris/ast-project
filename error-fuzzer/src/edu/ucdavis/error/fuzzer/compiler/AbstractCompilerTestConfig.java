package edu.ucdavis.error.fuzzer.compiler;

public abstract class AbstractCompilerTestConfig {

  private final String compiler;

  private final String flags;

  private final String cmd;

  private final boolean isCppCompiler;

  protected AbstractCompilerTestConfig(String compiler,
      final boolean isCppCompiler, boolean enableWarningFlags,
      boolean enableFatalError, String extraFlags) {
    super();
    this.isCppCompiler = isCppCompiler;
    this.compiler = compiler;

    this.flags = this.buildFlags(enableWarningFlags, enableFatalError,
        extraFlags);
    this.cmd = this.compiler + " " + this.flags + " ";
  }

  public boolean isCppCompiler() {
    return isCppCompiler;
  }

  private String buildFlags(boolean enableAllWarningFlags,
      boolean enableFatalError, String extraFlags) {
    final StringBuilder builder = new StringBuilder();
    if (enableAllWarningFlags) {
      final String allWarningFlags = this.getAllWarningFlags();
      if (allWarningFlags != null && allWarningFlags.length() > 0) {
        builder.append(" ").append(allWarningFlags).append(" ");
      }
    } else {
      final String disableWarningFlag = this.getDisableWarningFlags();
      if (disableWarningFlag != null && disableWarningFlag.length() > 0) {
        builder.append(" ").append(disableWarningFlag).append(" ");
      }
    }
    if (enableFatalError) {
      final String fatalErrorFlag = this.getFatalErrorFlag();
      if (fatalErrorFlag != null && fatalErrorFlag.length() > 0) {
        builder.append(" ").append(fatalErrorFlag).append(" ");
      }
    }
    builder.append(" ").append(extraFlags);
    return builder.toString();
  }

  protected abstract String getDisableWarningFlags();

  protected abstract String getFatalErrorFlag();

  protected abstract String getAllWarningFlags();

  public String getCompilerCmd() {
    return this.cmd;
  }

  public String getCompiler() {
    return compiler;
  }

  public String getFlags() {
    return flags;
  }

  public abstract boolean doesCompilerCrash(String compilerOutput);

}
