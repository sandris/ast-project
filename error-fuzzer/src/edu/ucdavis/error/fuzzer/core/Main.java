package edu.ucdavis.error.fuzzer.core;

import com.beust.jcommander.JCommander;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.core.engine.MultistageRandomTokenManipulationFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.engine.RandomTokenManipulationFuzzingEngine;
import edu.ucdavis.error.fuzzer.repository.SeedRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository;
import org.pmw.tinylog.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

  public static class FileWithSize implements Comparable<FileWithSize> {

    private final File file;

    public final int size;

    public FileWithSize(File file) {
      this.file = file;
      this.size = computeWordCount(file);
    }

    private int computeWordCount(File file2) {
      int count = 0;
      try (BufferedReader reader = new BufferedReader(new FileReader(file2))) {
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
          count += line.split("\\\\s+").length;
        }
        return count;
      } catch (IOException e) {
        Logger.error(e);
        throw new RuntimeException(e);
      }
    }

    @Override
    public int compareTo(FileWithSize o) {
      return Integer.compare(this.size, o.size);
    }
  }

  public static void main(String[] args) {
    final CommandOptions cmd = CommandOptions.v();
    JCommander commander = new JCommander(cmd, args);

    if (cmd.isHelp()) {
      commander.usage();
      return;
    }
    final File folder = cmd.getSeedFolder();
    if (!folder.exists() || !folder.isDirectory()) {
      throw new RuntimeException(
          "The seed folder " + folder + " does not exist or is not a " + "directory");
    }
    if (!cmd.fuzzingEnginesSupported()) {
      throw new RuntimeException( "Unsupported fuzzing engine");
    }

    Logger.info("constructing seed repository. ");
    final SeedRepository seedRepository =
        new SeedRepository.Builder(folder)
            .filter(f -> SourceFile.isCppSourceFile(f.getName()))
            .discardLargeFiles(1024 * 200)
            .shuffle()
            //            .limit(400)
            .sample(cmd.getSeedSamplingRatio())
            .build();

    final File expFolder = new File(cmd.getRunFolder());
    expFolder.mkdirs();
    Logger.info("the run folder is {}", expFolder);
    // convert seed programs to tokenized programs.
    Logger.info("constructing tokenized program repository");
    final TokenizedProgramRepository tokenizedProgramRepository =
        TokenizedProgramRepository.createRepository(
            seedRepository.getSeeds(), new File(expFolder, "tokenized-programs"));

    FuzzingLoop.createFuzzingLoop(
            expFolder,
            tokenizedProgramRepository)
        .run();
  }
}
