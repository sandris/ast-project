package edu.ucdavis.error.fuzzer.core.engine;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.core.CommandOptions;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.mutator.DeletionMutant;
import edu.ucdavis.error.fuzzer.mutator.InsertionMutant;
import edu.ucdavis.error.fuzzer.mutator.ReplacementMutant;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public class MultistageRandomTokenManipulationFuzzingEngine extends RandomTokenManipulationFuzzingEngine {


    public MultistageRandomTokenManipulationFuzzingEngine(
            TokenizedProgramRepository.SourceProgramWithTokens seedFile,
            File rootFolder,
            CompilerErrorRepository errorRepo,
            CrashingBugPackager crashingBugPackager)
            throws IOException, ParsingFailureException {
        super(seedFile, rootFolder, errorRepo, crashingBugPackager);
    }

    private final int mutantsUpperBound = CommandOptions.v().getUpperBoundMutants();

    public final LinkedList<AbstractMutant> pending_mutants = new LinkedList<AbstractMutant>();

    @Override
    protected boolean hasMoreMutants() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    private AbstractMutant mutateBySubstitution(ImmutableList<TokenProgramSegment> seed) {
        final int tokenCount = seed.size();
        final int sourceIndex = this.random.nextInt(tokenCount);
        int targetIndex;
        for (targetIndex = this.random.nextInt(tokenCount);
             targetIndex != sourceIndex;
             targetIndex = this.random.nextInt(tokenCount)) {}
        return new ReplacementMutant(seed, targetIndex, seed.get(sourceIndex));
    }

    private AbstractMutant mutateByDeletion(ImmutableList<TokenProgramSegment> tokenList) {
        return new DeletionMutant(tokenList, this.random.nextInt(tokenList.size()));
    }

    private AbstractMutant mutateByInsertion(ImmutableList<TokenProgramSegment> mutant) {
        assert (mutant.size() > 1);

        int sourceIndex;
        int targetIndex;
        do {
            sourceIndex = this.random.nextInt(mutant.size());
            targetIndex = this.random.nextInt(mutant.size() + 1);
        } while (sourceIndex == targetIndex);

        return new InsertionMutant(mutant, targetIndex, mutant.get(sourceIndex));
    }

    @Override
    public void doesNotGenerateError(AbstractMutant mutant) {
        pending_mutants.add(mutant);
    }

    @Override
    public void doesGenerateError(AbstractMutant mutant) {
        pending_mutants.add(mutant);
    }

    @Override
    public boolean canDeepen() {
        return !pending_mutants.isEmpty();
    }

    @Override
    protected AbstractMutant mutate(ImmutableList<TokenProgramSegment> tokens) {
        while (!this.pending_mutants.isEmpty()) {
            AbstractMutant m = this.pending_mutants.getFirst();
            this.pending_mutants.removeFirst();
            // Ugly hack.
            ImmutableList<TokenProgramSegment> l = m.getAlignedMutant().stream().filter(x -> x instanceof TokenProgramSegment).map(
                    x -> (TokenProgramSegment) x).collect(
                    collectingAndThen(toList(), ImmutableList::copyOf)
            );
            if (!l.isEmpty())
                return this.internalMutate(l);
        }

        return this.internalMutate(tokens);

    }

    @Override
    protected AbstractMutant internalMutate(ImmutableList<TokenProgramSegment> tokens) {
        // final ArrayList<IProgramSegment> mutant = new
        // ArrayList<>(this.originalProgram);
        final int choice = this.random.nextInt(3);
        switch (choice) {
            case 0:
                return this.mutateBySubstitution(tokens);
            case 1:
                return this.mutateByDeletion(tokens);
            case 2:
                return this.mutateByInsertion(tokens);
            default:
                throw new AssertionError("Cannot reach here. ");
        }
    }
}
