package edu.ucdavis.error.fuzzer.core.engine;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.CommandOptions;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import edu.ucdavis.error.fuzzer.mutator.SeedMutant;

import edu.ucdavis.error.fuzzer.token.TokenUtility;

import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;

import java.util.ArrayList;

import java.io.File;
import java.io.IOException;

public class CharacterFuzzingEngine extends AbstractFuzzingEngine {

  private static final Character insertChars[] = {'\0', 'a', 'f', 'k', 'u', '0', '4', '.', ',', '[', ']', '{', '}', '(', ')', ';', ':', '\\', '/', '"', '\'', '?', '>', '<', '|', '`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '-', '_', '+', '=', 'ç', ' ', '\t', '\n', '\r'};

  private class Mutant extends AbstractMutant {
    private class Segment implements IProgramSegment {
      final String segment;
      Segment(String segment) {
        this.segment = segment;
      }

      @Override
      public String getLexeme() {
        return this.segment;
      }
    }

    final ArrayList<IProgramSegment> mutant;
    final ArrayList<IProgramSegment> seed;

    Mutant(ImmutableList<TokenProgramSegment> tokens, String prefix, String seed, String mutant, String suffix) {
      super(new SeedMutant(tokens));
      final Segment prefixSegment = new Segment(prefix);
      final Segment seedSegment = new Segment(seed);
      final Segment mutantSegment = new Segment(mutant);
      final Segment suffixSegment = new Segment(suffix);
      this.mutant = new ArrayList<IProgramSegment>();
      this.mutant.add(prefixSegment);
      this.mutant.add(mutantSegment);
      this.mutant.add(suffixSegment);
      this.seed = new ArrayList<IProgramSegment>();
      this.mutant.add(prefixSegment);
      this.mutant.add(seedSegment);
      this.mutant.add(suffixSegment);
    }

    @Override
    public ArrayList<IProgramSegment> getAlignedSeed() {
      return this.seed;
    }

    @Override
    public ArrayList<IProgramSegment> getAlignedMutant() {
      return this.mutant;
    }
  }

  public CharacterFuzzingEngine(
      SourceProgramWithTokens seedFile,
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager)
      throws IOException, ParsingFailureException {
    super(seedFile, rootFolder, errorRepo, crashingBugPackager);
  }

  private final int mutantsUpperBound = CommandOptions.v().getUpperBoundMutants();

  @Override
  protected boolean hasMoreMutants() {
    throw new UnsupportedOperationException("Not implemented yet");
  }

  private AbstractMutant mutateBySubstitution(String seed, ImmutableList<TokenProgramSegment> tokens) {
    int length = seed.length();
    int idx = this.random.nextInt(length);
    int optionLength = CharacterFuzzingEngine.insertChars.length;
    char character = CharacterFuzzingEngine.insertChars[this.random.nextInt(optionLength)];

    final String prefix = seed.substring(0, idx);
    final String oldChar = seed.substring(idx, idx + 1);
    final String newChar = String.valueOf(character);
    final String suffix = seed.substring(idx + 1, length);
    return new Mutant(tokens, prefix, oldChar, newChar, suffix);
  }

  private AbstractMutant mutateByDeletion(String seed, ImmutableList<TokenProgramSegment> tokens) {
    int length = seed.length();
    int idx = this.random.nextInt(length);
    final String prefix = seed.substring(0, idx);
    final String delta = seed.substring(idx, idx + 1);
    final String suffix = seed.substring(idx + 1, length);
    return new Mutant(tokens, prefix, delta, "", suffix);
  }

  private AbstractMutant mutateByInsertion(String seed, ImmutableList<TokenProgramSegment> tokens) {
    int length = seed.length();
    int idx = this.random.nextInt(length + 1);
    int optionLength = CharacterFuzzingEngine.insertChars.length;
    char character = CharacterFuzzingEngine.insertChars[this.random.nextInt(optionLength)];
    final String prefix = seed.substring(0, idx);
    final String delta = String.valueOf(character);
    final String suffix = seed.substring(idx, length);
    return new Mutant(tokens, prefix, "", delta, suffix);
  }

  @Override
  protected AbstractMutant internalMutate(ImmutableList<TokenProgramSegment> tokens) {
    // final ArrayList<IProgramSegment> mutant = new
    // ArrayList<>(this.originalProgram);
    final int choice = this.random.nextInt(3);
    final StringBuilder builder = new StringBuilder();
    tokens.forEach(t -> builder.append(t.getLexeme()).append("\n"));
    final String seed = builder.toString();
    switch (choice) {
      case 0:
        return this.mutateBySubstitution(seed, tokens);
      case 1:
        return this.mutateByDeletion(seed, tokens);
      case 2:
        return this.mutateByInsertion(seed, tokens);
      default:
        throw new AssertionError("Cannot reach here. ");
    }
  }
}
