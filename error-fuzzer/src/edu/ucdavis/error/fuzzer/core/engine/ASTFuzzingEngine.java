package edu.ucdavis.error.fuzzer.core.engine;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.CommandOptions;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import edu.ucdavis.error.fuzzer.mutator.SeedMutant;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;

import org.apache.commons.io.FileUtils;

import com.google.common.base.Charsets;

import edu.ucdavis.error.fuzzer.token.cdt.CDTParser;
import org.eclipse.cdt.core.dom.ast.IASTFileLocation;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;

import java.util.ArrayList;

import java.io.File;
import java.io.IOException;

public class ASTFuzzingEngine extends AbstractFuzzingEngine {

  private class Mutant extends AbstractMutant {
    private class Segment implements IProgramSegment {
      final String segment;
      Segment(String segment) {
        this.segment = segment;
      }

      @Override
      public String getLexeme() {
        return this.segment;
      }
    }

    final ArrayList<IProgramSegment> mutant;
    final ArrayList<IProgramSegment> seed;

    Mutant(ImmutableList<TokenProgramSegment> tokens, String prefix, String seed, String mutant, String suffix) {
      super(new SeedMutant(tokens));
      final Segment prefixSegment = new Segment(prefix);
      final Segment seedSegment = new Segment(seed);
      final Segment mutantSegment = new Segment(mutant);
      final Segment suffixSegment = new Segment(suffix);
      this.mutant = new ArrayList<IProgramSegment>();
      this.mutant.add(prefixSegment);
      this.mutant.add(mutantSegment);
      this.mutant.add(suffixSegment);
      this.seed = new ArrayList<IProgramSegment>();
      this.mutant.add(prefixSegment);
      this.mutant.add(seedSegment);
      this.mutant.add(suffixSegment);
    }

    @Override
    public ArrayList<IProgramSegment> getAlignedSeed() {
      return this.seed;
    }

    @Override
    public ArrayList<IProgramSegment> getAlignedMutant() {
      return this.mutant;
    }
  }

  public ASTFuzzingEngine(
      SourceProgramWithTokens seedFile,
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager)
      throws IOException, ParsingFailureException {
    super(seedFile, rootFolder, errorRepo, crashingBugPackager);
  }

  private final int mutantsUpperBound = CommandOptions.v().getUpperBoundMutants();

  @Override
  protected boolean hasMoreMutants() {
    throw new UnsupportedOperationException("Not implemented yet");
  }

  private IASTTranslationUnit getRoot(String seed) {
    File tempFile = null;
    try {
      tempFile = File.createTempFile("ast-fuzz", "parse");
      FileUtils.writeStringToFile(tempFile, seed, Charsets.UTF_8, false);
      IASTTranslationUnit translationUnit = new CDTParser().parse(new SourceFile(tempFile));
      return translationUnit;
    } catch (ParsingFailureException e) {
      if (tempFile != null) tempFile.delete();
      return null;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private ArrayList<IASTNode> getChildren(IASTNode root, boolean requireMultichild, int depth) {
    ArrayList<IASTNode> result = new ArrayList<IASTNode>();
    if (!requireMultichild || root.getChildren().length >= 2) result.add(root);
    if (depth <= 0) return result;
    for (IASTNode child: root.getChildren()) {
      result.addAll(getChildren(child, requireMultichild, depth - 1));
    }
    return result;
  }

  private IASTNode randomChild(String seed, boolean requireMultichild) {
    IASTTranslationUnit root = getRoot(seed);
    if (root == null) return null;
    ArrayList<IASTNode> nodes = getChildren(root, requireMultichild, 100);
    int length = nodes.size();
    if (length == 0) return null;
    int idx = this.random.nextInt(length);
    return nodes.get(idx);
  }

  private AbstractMutant mutateBySwap(String seed, ImmutableList<TokenProgramSegment> tokens) {
    IASTNode parrent = randomChild(seed, true);
    if (parrent == null) return null;
    IASTNode children[] = parrent.getChildren();
    int idx = this.random.nextInt(children.length - 1);
    IASTNode firstNode = children[idx];
    IASTNode secondNode = children[idx+1];

    IASTFileLocation firstLocation = firstNode.getFileLocation();
    IASTFileLocation secondLocation = secondNode.getFileLocation();
    if (firstLocation == null) return null;
    if (secondLocation == null) return null;

    int firstStart = firstLocation.getNodeOffset();
    int firstEnd = firstStart + firstLocation.getNodeLength();
    int secondStart = secondLocation.getNodeOffset();
    int secondEnd = secondStart + secondLocation.getNodeLength();
    if (secondStart < firstEnd) secondStart = firstEnd;
    if (secondEnd < secondStart) secondEnd = secondStart;

    final String prefix = seed.substring(0, firstStart);
    final String first = seed.substring(firstStart, firstEnd);
    final String mid = seed.substring(firstEnd, secondStart);
    final String second = seed.substring(secondStart, secondEnd);
    final String suffix = seed.substring(secondEnd, seed.length());
    return new Mutant(tokens, prefix, first + mid + second, second + mid + first, suffix);
  }

  private AbstractMutant mutateByDeletion(String seed, ImmutableList<TokenProgramSegment> tokens) {
    IASTNode node = randomChild(seed, false);
    if (node == null) return null;
    IASTFileLocation location = node.getFileLocation();
    if (location == null) return null;

    int start = location.getNodeOffset();
    int end = start + location.getNodeLength();
    final String prefix = seed.substring(0, start);
    final String delta = seed.substring(start, end);
    final String suffix = seed.substring(end, seed.length());
    return new Mutant(tokens, prefix, delta, "", suffix);
  }

  private AbstractMutant mutateByDuplication(String seed, ImmutableList<TokenProgramSegment> tokens) {
    IASTNode node = randomChild(seed, false);
    if (node == null) return null;
    IASTFileLocation location = node.getFileLocation();
    if (location == null) return null;

    int start = location.getNodeOffset();
    int end = start + location.getNodeLength();
    final String prefix = seed.substring(0, start);
    final String delta = seed.substring(start, end);
    final String suffix = seed.substring(start, seed.length());
    return new Mutant(tokens, prefix, "", delta, suffix);
  }

  @Override
  protected AbstractMutant internalMutate(ImmutableList<TokenProgramSegment> tokens) {
    // final ArrayList<IProgramSegment> mutant = new
    // ArrayList<>(this.originalProgram);
    final int choice = this.random.nextInt(3);
    final StringBuilder builder = new StringBuilder();
    tokens.forEach(t -> builder.append(t.getLexeme()).append("\n"));
    final String seed = builder.toString();
    switch (choice) {
      case 0:
        return this.mutateBySwap(seed, tokens);
      case 1:
        return this.mutateByDeletion(seed, tokens);
      case 2:
        return this.mutateByDuplication(seed, tokens);
      default:
        throw new AssertionError("Cannot reach here. ");
    }
  }
}
