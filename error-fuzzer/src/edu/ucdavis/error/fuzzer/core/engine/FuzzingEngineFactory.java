package edu.ucdavis.error.fuzzer.core.engine;

import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;

import java.io.File;
import java.io.IOException;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;

public class FuzzingEngineFactory {

  private final Class<? extends AbstractFuzzingEngine> klass;

  public FuzzingEngineFactory(Class<? extends AbstractFuzzingEngine> klass) {
    super();
    this.klass = klass;
  }

  public AbstractFuzzingEngine createEngine(
      SourceProgramWithTokens seedFile,
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager) throws IOException, ParsingFailureException {

    if (this.klass == RandomTokenManipulationFuzzingEngine.class) {
      return new RandomTokenManipulationFuzzingEngine(seedFile, rootFolder, errorRepo, crashingBugPackager);
    } else if (this.klass == MultistageRandomTokenManipulationFuzzingEngine.class) {
      return new MultistageRandomTokenManipulationFuzzingEngine(seedFile, rootFolder, errorRepo, crashingBugPackager);
    } else if (this.klass == CharacterFuzzingEngine.class) {
      return new CharacterFuzzingEngine(seedFile, rootFolder, errorRepo, crashingBugPackager);
    } else if (this.klass == ASTFuzzingEngine.class) {
      return new ASTFuzzingEngine(seedFile, rootFolder, errorRepo, crashingBugPackager);
    } else if (this.klass == NoerrorMultistageRandomTokenManipulationFuzzingEngine.class) {
      return new NoerrorMultistageRandomTokenManipulationFuzzingEngine(seedFile, rootFolder, errorRepo, crashingBugPackager);
    } else if (this.klass == ErrorMultistageRandomTokenManipulationFuzzingEngine.class) {
      return new ErrorMultistageRandomTokenManipulationFuzzingEngine(seedFile, rootFolder, errorRepo, crashingBugPackager);
    }

    throw new RuntimeException("unhandled engine type: " + this.klass);
  }
}
