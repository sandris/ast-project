package edu.ucdavis.error.fuzzer.core.engine;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.CommandOptions;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.mutator.DeletionMutant;
import edu.ucdavis.error.fuzzer.mutator.InsertionMutant;
import edu.ucdavis.error.fuzzer.mutator.ReplacementMutant;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

import java.io.File;
import java.io.IOException;

public class RandomTokenManipulationFuzzingEngine extends AbstractFuzzingEngine {

  public RandomTokenManipulationFuzzingEngine(
      SourceProgramWithTokens seedFile,
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager)
      throws IOException, ParsingFailureException {
    super(seedFile, rootFolder, errorRepo, crashingBugPackager);
  }

  private final int mutantsUpperBound = CommandOptions.v().getUpperBoundMutants();

  @Override
  protected boolean hasMoreMutants() {
    throw new UnsupportedOperationException("Not implemented yet");
  }

  private AbstractMutant mutateBySubstitution(ImmutableList<TokenProgramSegment> seed) {
    final int tokenCount = seed.size();
    final int sourceIndex = this.random.nextInt(tokenCount);
    int targetIndex;
    for (targetIndex = this.random.nextInt(tokenCount);
        targetIndex != sourceIndex;
        targetIndex = this.random.nextInt(tokenCount)) {}
    return new ReplacementMutant(seed, targetIndex, seed.get(sourceIndex));
  }

  private AbstractMutant mutateByDeletion(ImmutableList<TokenProgramSegment> tokenList) {
    return new DeletionMutant(tokenList, this.random.nextInt(tokenList.size()));
  }

  private AbstractMutant mutateByInsertion(ImmutableList<TokenProgramSegment> mutant) {
    assert (mutant.size() > 1);

    int sourceIndex;
    int targetIndex;
    do {
      sourceIndex = this.random.nextInt(mutant.size());
      targetIndex = this.random.nextInt(mutant.size() + 1);
    } while (sourceIndex == targetIndex);

    return new InsertionMutant(mutant, targetIndex, mutant.get(sourceIndex));
  }

  @Override
  protected AbstractMutant internalMutate(ImmutableList<TokenProgramSegment> tokens) {
    // final ArrayList<IProgramSegment> mutant = new
    // ArrayList<>(this.originalProgram);
    final int choice = this.random.nextInt(3);
    switch (choice) {
      case 0:
        return this.mutateBySubstitution(tokens);
      case 1:
        return this.mutateByDeletion(tokens);
      case 2:
        return this.mutateByInsertion(tokens);
      default:
        throw new AssertionError("Cannot reach here. ");
    }
  }
}
