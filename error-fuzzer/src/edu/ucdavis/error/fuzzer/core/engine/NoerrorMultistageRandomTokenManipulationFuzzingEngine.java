package edu.ucdavis.error.fuzzer.core.engine;

import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository;

import java.io.File;
import java.io.IOException;

public class NoerrorMultistageRandomTokenManipulationFuzzingEngine extends MultistageRandomTokenManipulationFuzzingEngine {
    public NoerrorMultistageRandomTokenManipulationFuzzingEngine(TokenizedProgramRepository.SourceProgramWithTokens seedFile, File rootFolder, CompilerErrorRepository errorRepo, CrashingBugPackager crashingBugPackager) throws IOException, ParsingFailureException {
        super(seedFile, rootFolder, errorRepo, crashingBugPackager);
    }

    @Override
    public void doesGenerateError(AbstractMutant mutant) {

    }
}
