package edu.ucdavis.error.fuzzer.core;

import edu.ucdavis.error.fuzzer.core.engine.*;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.util.RandomUtil;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FuzzingLoop {

  public static FuzzingLoop createFuzzingLoop(
      File rootFolder,
      TokenizedProgramRepository tokenizedProgramRepository) {
    final CommandOptions cmd = CommandOptions.v();

    final CompilerErrorRepository errorRepo =
        CompilerErrorRepository.createOrLoadErrorRepository(
            new File(rootFolder, "error/" + cmd.getEngineType()), cmd.getMaxInstancesPerErrorMsg());
    final CrashingBugPackager crashingBugPackager =
        new CrashingBugPackager(new File(rootFolder, "crash/" + cmd.getEngineType()));

    int engineCount = cmd.engineTypeCount();
    FuzzingEngineFactory[] engineFactories = new FuzzingEngineFactory[engineCount];
    for (int i = 0; i < engineCount; i++) {
      Class<? extends AbstractFuzzingEngine> engineClass;
      if (cmd.isTokenFuzzingEngine(i)) {
        engineClass = RandomTokenManipulationFuzzingEngine.class;
      } else if (cmd.isMultistageFuzzingEngine(i)) {
        engineClass = MultistageRandomTokenManipulationFuzzingEngine.class;
      } else if (cmd.isCharacterFuzzingEngine(i)) {
        engineClass = CharacterFuzzingEngine.class;
      } else if (cmd.isASTFuzzingEngine(i)) {
        engineClass = ASTFuzzingEngine.class;
      } else if (cmd.isErrorMultistageFuzzingEngine(i)) {
        engineClass = ErrorMultistageRandomTokenManipulationFuzzingEngine.class;
      } else if (cmd.isNoerrorMultistageFuzzingEngine(i)) {
        engineClass = NoerrorMultistageRandomTokenManipulationFuzzingEngine.class;
      } else {
        throw new RuntimeException("Unimplemented fuzzing engine");
      }
      Logger.info("Using engine type {}", engineClass);

      engineFactories[i] = new FuzzingEngineFactory(engineClass);
    }
    return new FuzzingLoop(
        rootFolder, errorRepo, crashingBugPackager, tokenizedProgramRepository, engineFactories);
  }

  private final List<AbstractFuzzingEngine> fuzzingTasks;

  private FuzzingLoop(
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager,
      TokenizedProgramRepository tokenizedProgramRepository,
      FuzzingEngineFactory[] factories) {
    this.fuzzingTasks = new ArrayList<>();
    for (SourceProgramWithTokens program : tokenizedProgramRepository.getPrograms()) {
      for (FuzzingEngineFactory factory: factories) {
        try {
          final AbstractFuzzingEngine engine =
              factory.createEngine(
                  program, rootFolder, errorRepo, crashingBugPackager);
          if (!engine.preRunCheck()) {
            Logger.info("The preRunCheck failed for source file {}", program.getSourceFile());
            continue;
          }
          this.fuzzingTasks.add(engine);
        } catch (Throwable e) {
          throw new AssertionError("Exception is not expected here. ", e);
        }
      }
    }
    Logger.info("Created {} fuzzing engines", fuzzingTasks.size());
  }

  public void run() {
    while (true) {
      for (AbstractFuzzingEngine engine : this.fuzzingTasks) {
        final int iterations = 10 * (1 + RandomUtil.v().nextInt(10));
        Logger.info("Run mutation engine for {} iterations", iterations);
        engine.run(iterations);
      }
    }
  }
}
