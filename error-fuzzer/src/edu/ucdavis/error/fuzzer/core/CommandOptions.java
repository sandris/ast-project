package edu.ucdavis.error.fuzzer.core;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;

import java.io.File;

public class CommandOptions {

  private static final CommandOptions INSTANCE = new CommandOptions();

  @Parameter(
    names = {"--sort-seeds"},
    description = "to sort seeds: size (increasing), random"
  )
  private String sortSeed = "size";

  @Parameter(
    names = {"--engine"},
    description = "comma separated list of fuzzing engines to use: token (random tokens), multistage (multiple stages of 'token') (variants: err-multistage, noerr-multistage), character (random characters), ast (AST level fuzzing)"
  )
  private String engineType = "token";

  @Parameter(
    names = {"--compiler-flags"},
    description = "additional compiler flags"
  )
  private String compilerFlags = "";


  @Parameter(
    names = {"--run-folder"},
    description = "the folder to run this tool. All the data will be stored in this folder."
  )
  private String runFolder = "exp-run";

  @Parameter(
    names = {"--seed-sample-ratio"},
    description = ""
  )
  private double seedSamplingRatio = 1;

  @Parameter(
    names = {"--seed-folder"},
    description = "the folder of seed programs",
    converter = FileConverter.class
  )
  private File seedFolder = new File("gcc-seed-programs");

  @Parameter(
    names = {"--num-threads"},
    description = "the number of threads to parallelize the task"
  )
  private int numThreads = 9;

  @Parameter(
    names = {"-max-instances-per-error-msg"},
    description = "the allowed maximum number of instances per error message"
  )
  private int maxInstancesPerErrorMsg = 20;

  @Parameter(names = "--ub-mutants-per-seed", description = "the upper bound of mutants per seed")
  private int upperBoundMutants = 8000;

  @Parameter(names = "--help", help = true)
  private boolean help;

  @Parameter(
    names = "--timeout",
    description = "the time budget for compilation tasks (in seconds)"
  )
  private int timeoutSeconds = 30;

  @Parameter(names = "--test-ccomp", description = "whether to test CompCert for crashing bugs")
  private boolean testingCompCert = false;

  @Parameter(names = "--test-clang", description = "whether to test clang-trunk for crashing bugs")
  private boolean testingClangTrunkForCrashes = false;

  @Parameter(names = "--test-gcc", description = "whether to test gcc-trunk for crashing bugs")
  private boolean testingGccTrunkForCrashes = false;

  private CommandOptions() {}

  private static boolean contains(String[] arr, String needle) {
      for (String itter: arr) {
          if (needle.equals(itter)) return true;
      }
      return false;
  }

  public static CommandOptions v() {
    return INSTANCE;
  }

  public File getSeedFolder() {
    return seedFolder;
  }

  public void setSeedFolder(File seedFolder) {
    this.seedFolder = seedFolder;
  }

  public String getSeedSorting() {
    return this.sortSeed;
  }

  public int getNumThreads() {
    return numThreads;
  }

  public void setNumThreads(int numThreads) {
    this.numThreads = numThreads;
  }

  public void setTimeoutSeconds(int timeoutSeconds) {
    this.timeoutSeconds = timeoutSeconds;
  }

  public double getSeedSamplingRatio() {
    return seedSamplingRatio;
  }

  public void setSeedSamplingRatio(double seedSamplingRatio) {
    this.seedSamplingRatio = seedSamplingRatio;
  }

  public boolean isHelp() {
    return help;
  }

  public void setHelp(boolean help) {
    this.help = help;
  }

  public int getUpperBoundMutants() {
    return upperBoundMutants;
  }

  public void setUpperBoundMutants(int upperBoundMutants) {
    this.upperBoundMutants = upperBoundMutants;
  }

  public boolean isTokenFuzzingEngine(int i) {
    return this.engineType(i).equals("token");
  }

  public boolean isCharacterFuzzingEngine(int i) {
    return this.engineType(i).equals("character");
  }

  public boolean isMultistageFuzzingEngine(int i) {
    return this.engineType(i).equals("multistage");
  }

  public boolean isErrorMultistageFuzzingEngine(int i) {
    return this.engineType(i).equals("err-multistage");
  }

  public boolean isNoerrorMultistageFuzzingEngine(int i) {
    return this.engineType(i).equals("noerr-multistage");
  }

  public boolean isASTFuzzingEngine(int i) {
    return this.engineType(i).equals("ast");
  }
  
  private String engineType(int i) {
    return getEngineType().split(",")[i];
  }

  public int engineTypeCount() {
    return getEngineType().split(",").length;
  }

  public boolean fuzzingEnginesSupported() {
    String[] engines = getEngineType().split(",");
    String[] supported = {"token", "ast", "character", "multistage", "err-multistage", "noerr-multistage"};
    for (String engine: engines) {
      if (!contains(supported, engine)) return false;
    }
    return engines.length > 0;
  }

  public String getEngineType() {
    return engineType;
  }

  public void setEngineType(String engineType) {
    this.engineType = engineType;
  }

  public String getCompilerFlags() {
    return compilerFlags;
  }

  public void setCompilerFlags(String compilerFlags) {
    this.compilerFlags = compilerFlags;
  }

  public int getTimeoutThreshold() {
    return this.timeoutSeconds;
  }

  public boolean isTimeoutEnabled() {
    return this.timeoutSeconds < Integer.MAX_VALUE && this.timeoutSeconds > 0;
  }

  public boolean testingCompCert() {
    return this.testingCompCert;
  }

  public boolean testingClangForCrashes() {
    return this.testingClangTrunkForCrashes;
  }

  public boolean testingGccForCrashes() {
    return this.testingGccTrunkForCrashes;
  }

  public int getMaxInstancesPerErrorMsg() {
    return maxInstancesPerErrorMsg;
  }

  public String getRunFolder() {
    return runFolder;
  }
}
