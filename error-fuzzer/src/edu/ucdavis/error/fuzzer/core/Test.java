package edu.ucdavis.error.fuzzer.core;

import java.io.IOException;

import org.apache.commons.exec.ExecuteException;

public class Test {

  public static void main(String[] args) throws ExecuteException, IOException {
    // final CmdOutput output = Shell.run(TimeoutUtil.getTimeoutCmd() + " -s 9 1
    // sleep 10");
    // System.out.println(output);
    Main.main(new String[] { "--seed-folder", "gcc-seed-programs", "--num-threads", "1" });
  }

}
