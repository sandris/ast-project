package edu.ucdavis.error.fuzzer.util;

import java.util.List;
import java.util.Random;

import com.google.common.base.Preconditions;

public class RandomUtil {

  private final Random rand = new Random();

  private final static RandomUtil INSTANCE = new RandomUtil();

  public static RandomUtil v() {
    return INSTANCE;
  }

  public int nextInt(int bound) {
    return this.rand.nextInt(bound);
  }

  public int nextInt() {
    return rand.nextInt();
  }

  public <T> T pick(List<T> list) {
    Preconditions.checkArgument(list.size() > 0);
    return list.get(this.nextInt(list.size()));
  }

  public Random getRandom() {
    return rand;
  }
}
