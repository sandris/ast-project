package edu.ucdavis.error.fuzzer.util;

import com.google.common.base.Charsets;
import edu.ucdavis.error.fuzzer.reducer.ErrorReducer;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.pmw.tinylog.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.google.common.base.Preconditions.checkState;
import static edu.ucdavis.error.fuzzer.core.Shell.*;

/** Created by Chengnian Sun on 4/30/17. */
public class ClangFormat {

  private static final String CMD_CLANG_FORMAT = "clang-format";

  private static final String CLANG_FORMAT_CONFIGURATION = loadClangFormatConfiguration();

  private static final boolean IS_CLANG_FORMAT_VERSION_ABOVE_5 = isClangFormatVersionAbove5();

  private static final boolean isClangFormatVersionAbove5() {
    CommandLine cmd = new CommandLine(CMD_CLANG_FORMAT);
    cmd.addArgument("-version");
    final CmdOutput result = run(cmd);
    final String stdout = result.getStdout();
    return stdout.startsWith("clang-format version 5")
        || stdout.startsWith("clang-format version 6")
        || stdout.startsWith("clang-format version 7")
        || stdout.startsWith("clang-format version 10")
        || stdout.startsWith("Debian clang-format version 11");
  }

  private static String loadClangFormatConfiguration() {
    InputStream configStream =
        ErrorReducer.class
            .getClassLoader()
            .getResourceAsStream(
                ClangFormat.class.getPackage().getName().replace('.', '/') + "/style.config.txt");
    checkState(configStream != null, "Cannot find the style.config.txt in the class path.");
    try (BufferedInputStream bis = new BufferedInputStream(configStream)) {
      return IOUtils.toString(bis, Charsets.UTF_8);
    } catch (Exception e) {
      Logger.error(e);
      throw new RuntimeException(e);
    }
  }

  public static CmdOutput formatSourceFile(File sourceFile) {
    Logger.debug("creating clang-format config file");
    final File clangFormatFile =
        new File(sourceFile.getParentFile().getAbsoluteFile(), ".clang-format");

    try {
      FileUtils.write(clangFormatFile, CLANG_FORMAT_CONFIGURATION, Charsets.UTF_8);
    } catch (IOException e) {
      Logger.error(e, "fail to write .clang-format to {}", clangFormatFile);
    }

    checkState(IS_CLANG_FORMAT_VERSION_ABOVE_5, "The %s's version is below 5", CMD_CLANG_FORMAT);
    CommandLine commandline = new CommandLine(CMD_CLANG_FORMAT);
    commandline.addArgument("-i", false);
    commandline.addArgument(sourceFile.getAbsolutePath());

    CmdOutput execResult = run(commandline);
    return execResult;
  }
}
