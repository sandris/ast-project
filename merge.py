import os
import sys
from shutil import copytree

if len(sys.argv) < 4:
    print("usage:", sys.argv[0], "dst src1 src2 [src3 [src4 [...]]]")
    exit(1)

dst = sys.argv[1]
srcs = sys.argv[2:]

os.makedirs(dst)

def copy(src, dst):
    for file in os.listdir(src):
        fullsrc = os.path.join(src, file)
        fulldst = os.path.join(dst, file)
        if not os.path.exists(fulldst):
            copytree(fullsrc, fulldst)
        elif os.path.isdir(fulldst) and os.path.isdir(fullsrc):
            copy(fullsrc, fulldst)
        else:
            print("collision on", fulldst)
            exit(1)

for src in srcs:
    copy(src, dst)