# ast-project

## Run on EULER
```bash
#!/usr/bin/bash
cd ~/
# git clone or unpack
git clone https://gitlab.ethz.ch/sandris/ast-project
# The path *must* be meta-compiler
mv ast-project meta-compiler
cp meta-compiler/*.py ~/
cp -r ~/meta-compiler/mods ~/
cd /cluster/scratch/$USER/
tar -xzvf ~/meta-compiler/error-fuzzer/gcc-seed-programs.tgz
module load new llvm/7.0.1
source ~/meta-compiler/comp-mods
cd ~/meta-compiler/error-fuzzer
bash ./build.sh
cd ~
# Example launch (must be run on Euler or comparable)
python3 launch.py /cluster/scratch/$USER/gcc-seed-programs/ 14 1800 memlimit-testrun1 2G
```


## Files
### analyse.py
A python script to count the number of unique error messages found by a fuzzing job. It can also be used to compare multiple runs.

#### Requirements
A JSON array with the supported error messages and warnings must be at ~/errors.json and ~/warnings.json respectively. These can be initialized with `[]` and then mostly be extended with the suggestions made by analyse.py by running `./use-suggested.sh`

The output directories of the analyzed runs must be in ~/mcruns

#### Usage
`python3 analyse.py <runid1> [<runid2> [...]]`

### clean-output.py
A python script to filter the lsf output files. It will delete any files that indicate a successfully completed job while indicating which jobs may have been unsucessful.

#### Usage
`python3 clean-output.py <file1> [<file2> [...]]`

### comp-mod
A shell script that loads the modules required to compile the error-fuzzer.

#### Usage
`. ~/meta-compiler/comp-mods`

### error-fuzzer
The source tree for the modified version of the error-fuzzer.

### euler_setup.sh
The shell script presented above to setup the environment on euler to run fuzzing jobs.

### gcc-llvm-seeds.zip
The combined gcc and clang test suites preprocessed to be usable as seeds for the error-fuzzer.

### gcc-seeds.zip
The gcc test suite preprocessed to be usable as seeds for the error-fuzzer.

### jre-8u333-linux-x64.tar.gz
A java runtime version that reliably works with the error-fuzzer.

### killall.py
A python script to kill all currently running LSF jobs.

#### Usage
`python3 killall.py`

### launch.py
A python script to launch a fuzzing job. It partitions the seeds and launches the necessary LSF jobs.

#### Requirements
The home directory must be setup as done by euler_setup.sh

#### Usage
`python3 launch.py <seeds> <#tasks> <duration> <id> <memory> [<args>]`

- seeds: path to the unpartitioned seeds
- #tasks: the number of tasks to launch
- duration: the number of seconds for which each of the tasks should be run
- id: unique string to identify the fuzzing job
- memory: the maximum number of gigabytes each task can use - specified as `<n>G`
- args: a JSON array of additional arguments passed to the error-fuzzer

### llvm-seeds.zip
The clang test suite preprocessed to be usable as seeds for the error-fuzzer.

### merge.py
A python script to merge multiple directory trees together.

#### Usage
`python3 merge.py <dst> <src1> <src2>`

### mods
A shell script that loads the modules required to run the error-fuzzer.

### monitor.py
A python script to monitor memory usage by running processes.

#### Usage
Don't. Used internally by wrapper.py

### partition.py
A python script to partition the seeds.

#### Usage
Don't. Used internally by launch.py

### README.md
This file.

### results.csv
The results of the fuzzing jobs run.

### stopjob.py
A python script to kill all LSF jobs associated with a particular fuzzing job.

#### Usage
`python3 stopjob.py <runid>`

### use-suggested.sh
A shell script to include error messages (and warnings) newly detected by analyse.py into the repository.

### wrapper.py
A python script to set up the environment for a fuzzing task.

#### Usage
Don't. Used internally by launch.py


# Sample Commands for Run Submission
```
python3 launch.py /cluster/scratch/$USER/gcc-seed-programs 14 259200 long-run{1-3} 2G
python3 launch.py /cluster/scratch/$USER/gcc-seed-programs 14 259200 long-compare-run{1-3} 2G

python3 launch.py /cluster/scratch/sandris/gcc-llvm-seeds/ 14 86400 character-run{1-3} 2G '["--engine","character"]'

python3 launch.py /cluster/scratch/$USER/gcc-llvm-seeds/ 14 86400 new-combined-run{1-3} 2G '["--engine","token,noerr-multistage,character"]'

python3 launch.py /cluster/scratch/$USER/gcc-llvm-seeds/ 14 86400 new-err-multistage-run{1-3} 2G '["--engine","err-multistage"]'
python3 launch.py /cluster/scratch/$USER/gcc-llvm-seeds/ 14 86400 new-noerr-multistage-run{1-3} 2G '["--engine","noerr-multistage"]'
python3 launch.py /cluster/scratch/$USER/gcc-llvm-seeds/ 14 86400 new-multistage-run{1-3} 2G '["--engine","multistage"]'

```
