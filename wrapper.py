from subprocess import Popen, TimeoutExpired
from shutil import copytree, rmtree
import sys
import os
import time
import signal
import json

if len(sys.argv) != 7:
    print("usage:", sys.argv[0], "seed-dir run-id workers-id duration memlimit args")
    exit(1)

seed = sys.argv[1]
runid = sys.argv[2]
workers = sys.argv[3].split(",")
duration = int(sys.argv[4])
args = json.loads(sys.argv[6])

workdir = os.environ["TMPDIR"]

bindir = os.path.join(workdir, "bin")
jrebin = os.path.join(os.environ["HOME"], "jre1.8.0_333/bin")
os.environ["PATH"] = bindir + ":" + jrebin + ":" + os.environ["PATH"]

os.mkdir(bindir)
for compiler in ["clang", "clang++", "clang-trunk", "clang++-trunk"]:
    path = os.path.join(bindir, compiler)
    open(path, "w").write(f"""#!/usr/bin/bash
ulimit -v {sys.argv[5]}
/cluster/apps/llvm/7.0.1/x86_64/bin/{compiler.split("-")[0]} "$@"
""")
    os.chmod(path, 0o500)

for worker in workers:
    copytree(os.path.join(seed, worker), os.path.join(workdir, "in", worker))

if "MONITOR_LOGFILE" in os.environ and len(os.environ["MONITOR_LOGFILE"]):
    _ = Popen([HOME + "/monitor.py", os.environ["MONITOR_LOGFILE"], str(duration)])

HOME = os.environ["HOME"]
popens = []
for worker in workers:
    cmd = HOME + "/meta-compiler/error-fuzzer/run-error-fuzzer.sh"
    cmd = [cmd, "--run-folder", os.path.join(workdir, "out", worker), "--seed-folder", os.path.join(workdir, "in", worker)]
    popens += [Popen(cmd + args, cwd=HOME + "/meta-compiler/error-fuzzer")]
try:
    for popen in popens:
        popen.wait(duration)
except TimeoutExpired:
    print("timeout expired")

print("kill fuzzer")
for popen in popens:
    popen.terminate()

error = 0
for popen in popens:
    while popen.poll() == None:
        time.sleep(1)
    if popen.poll() != 0:
        error = 1

for worker in workers:
    rmtree(os.path.join(workdir, "out", worker, "tokenized-programs"))
    copytree(os.path.join(workdir, "out", worker), os.path.join(HOME, "mcruns", runid, worker))
print("done:", [popen.poll() for popen in popens])
exit(error)
