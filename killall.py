import subprocess

def jobs():
    out = subprocess.run(["bjobs"], stdout=subprocess.PIPE, check=True).stdout
    r = [[y for y in x.split(" ") if y] for x in out.decode().split("\n") if x]
    header = r[0]
    r = r[1:]
    dicts = []
    for line in r:
        mapped = {}
        for i, h in enumerate(header):
            mapped[h] = line[i]
        dicts.append(mapped)
    return dicts

for job in jobs():
    print("killing", job["JOB_NAME"])
    subprocess.run(["bkill", job["JOBID"]], check=True)