#!/usr/bin/env python3

import sys
import os

args = sys.argv[1:]

prefix = "Exception in thread \"main\" java.lang.IllegalArgumentException: The program file"
suffix = "does not exist"

for file in args:
    lines = open(file, "r").read().split("\n")
    if "TERM_OWNER: job killed by owner." in lines[15:20]:
        os.unlink(file)
        continue
    for line in lines[-30:]:
        if lines[18] == "Exited with exit code 1." and line.startswith(prefix) and line.endswith(suffix):
           os.unlink(file)
           break
    else:
        print(file)
        print(lines[18])
