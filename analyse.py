import os
import sys
import glob
from shutil import copyfile
import json

if len(sys.argv) < 2:
    print("usage:", sys.argv[0], "run-id [run-id [...]]")
    exit(1)

HOME = os.environ["HOME"]

run_errors = []
run_warnings = []
for runid in sys.argv[1:]:
    print(runid)
    rundir = os.path.join(HOME, "mcruns", runid)
    err_msgs = []
    for file in glob.glob(rundir + "/*/error/*/unhandled_error_messages.txt"):
        err_msgs += [x for x in open(file, "r").read().split("\n") if len(x)]

    warn_msgs = []
    for file in glob.glob(rundir + "/*/error/*/unhandled_warning_messages.txt"):
        warn_msgs += [x for x in open(file, "r").read().split("\n") if len(x)]

    print("errors")
    print("msgs:", len(err_msgs))
    print("uniq:", len(set(err_msgs)))

    print("warnings")
    print("msgs:", len(warn_msgs))
    print("uniq:", len(set(warn_msgs)))

    errors = json.load(open(HOME + "/errors.json", "r"))
    warnings = json.load(open(HOME + "/warnings.json", "r"))

    def quote(e):
        if not len(e): return None
        if len(e) and e[-1] == "'": return "'"
        if len(e) and e[-1] == '"': return '"'
        return None

    def validate(esc, frag):
        if esc:
            return esc not in frag
        else:
            return frag.isnumeric()

    def get_fragments(error, msg):
        if not msg.startswith(error[0]):
            return None
        msg = msg[len(error[0]):]
        esc = quote(error[0])
        error = error[1:]
        if len(error) == 0:
            return [] if msg == "" else None
        if not msg.endswith(error[-1]):
            return None
        if len(error[-1]):
            msg = msg[:-len(error[-1])]
        error = error[:-1]
        result = []
        for comp in error:
            if comp not in msg:
                return None
            idx = msg.index(comp)
            if not validate(esc, msg[:idx]):
                return None
            result += [msg[:idx]]
            msg = msg[idx + len(comp):]
            esc = quote(comp)
        if not validate(esc, msg):
            return None
        return result + [msg]

    def get_start(msg):
        starts = "\"'0123456789"
        first_idx = len(msg)
        for c in starts:
            if c in msg:
                idx = msg.index(c)
                if idx < first_idx:
                    first_idx = idx
        return first_idx if first_idx < len(msg) else None

    def detect(msgs, predefined):
        result = {}
        detected = []
        for msg in set(msgs):
            for error in predefined:
                fragments = get_fragments(error, msg)
                if fragments != None:
                    encoded = "\n".join(error)
                    prev = result[encoded] if encoded in result else []
                    result[encoded] = prev + [fragments]
                    break
            else:
                if len([c for c in msg if c == "'"]) % 2 == 1:
                    print("Bad number of `'`:", msg)
                    exit(1)
                if len([c for c in msg if c == '"']) % 2 == 1:
                    print("Bad number of `\"`:", msg)
                    exit(1)
                for other in detected:
                    if get_fragments(other, msg):
                        break
                else:
                    start = get_start(msg)
                    pref = ""
                    error = []
                    while start != None:
                        if msg[start] == "'":
                            error += [pref + msg[:start + 1]]
                            msg = msg[start + 1:]
                            end = msg.index("'") + 1
                            pref = "'"
                        elif msg[start] == '"':
                            error += [pref + msg[:start + 1]]
                            msg = msg[start + 1:]
                            end = msg.index('"') + 1
                            pref = '"'
                        else:
                            error += [pref + msg[:start]]
                            end = start
                            while end < len(msg) and msg[end] in "0123456789":
                                end += 1
                            pref = ""
                        msg = msg[end:]
                        start = get_start(msg)
                    error += [pref + msg]
                    detected += [error]
        return result, detected

    final_errs, new_errs = detect(err_msgs, errors)
    final_warnings, new_warnings = detect(warn_msgs, warnings)

    print("\n")
    print("errors:", len(final_errs))
    print("suggested errors:", len(new_errs))
    json.dump(errors + new_errs, open(HOME + "/suggested-errors.json", "w"))
    print("\n")
    print("warnings:", len(final_warnings))
    print("suggested warnings:", len(new_warnings))
    json.dump(warnings + new_warnings, open(HOME + "/suggested-warnings.json", "w"))
    if (len(new_errs) or len(new_warnings)) and len(sys.argv) > 2:
        print('cannot diff with unhandled errors')
        exit(1)
    run_errors.append(final_errs)
    run_warnings.append(final_warnings)

if len(sys.argv) == 2:
    exit(0)

def printtable(data, names):
    assert len(data) == len(names)
    print("Name                          Total     New")
    print("==================================================")
    print(names[0].ljust(30) + str(len(data[0])).rjust(4))
    for i in range(1, len(data)):
        new = len([x for x in data[i] if x not in data[0]])
        print(names[i].ljust(30) + str(len(data[i])).rjust(4) + " " * 6 + str(new).rjust(4))

print("Warnings")
printtable(run_warnings, sys.argv[1:])

print("\n\n\nErrors")
printtable(run_errors, sys.argv[1:])