#!/usr/bin/env python3
import subprocess
import sys

def jobs():
    lines = subprocess.run(["bbjobs", "-P"], stdout=subprocess.PIPE, check=True).stdout.decode().split("\n")
    job = {}
    jobs = []
    for line in lines:
        if len(line) == 0:
            jobs.append(job)
            job = {}
        if "   " not in line:
            continue
        comps = [x.strip(": ") for x in line.split("   ") if len(x)]
        id, name, value = comps
        job[name] = value 
        assert id == job["Job ID"]
    if len(job):
        jobs.append(job)
    return jobs

for job in jobs():
    cmd = job["Command"].split()
    python, wrapper, seeds, run, worker, duration, ram, args = cmd
    assert python == "python3"
    assert wrapper == "wrapper.py"
    assert int(worker) < 100
    assert int(duration) > 100
    assert int(ram) >= 1000000
    assert args[0] == "[" and args[-1] == "]"
    if run != sys.argv[1]:
        continue
    subprocess.run(["bkill", job["Job ID"]], check=True)