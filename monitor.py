#!/usr/bin/env python3

import subprocess
import os
import sys
import datetime
import time

if len(sys.argv) not in [3, 4]:
    print("usage:", sys.argv[0], "logfile duration [min-size]")
    exit(1)

logfile = sys.argv[1]
duration = int(sys.argv[2])
min_size = 1
if len(sys.argv) == 4:
    min_size = float(sys.argv[3])

min_size *= 1024 * 1024

USER = os.environ["USER"]

def get_large_procs(bound = 1024*1024):
    sub = subprocess.run(["ps", "-u", USER, "-o", "rss,cmd"], capture_output=True)
    procs = [x.strip().split(" ") for x in sub.stdout.decode().split("\n") if x]
    procs = [(int(x[0]), " ".join(x[1:])) for x in procs[1:] if int(x[0]) >= bound]
    return procs

f = open(logfile, "w")
start = time.time()
while time.time() - start < duration:
    for mem, cmd in get_large_procs(min_size):
        ts = datetime.datetime.now().isoformat()
        f.write(f"{ts} {str(mem).zfill(8)} {cmd}\n")
    time.sleep(1)
